export  default function slidersInit(){
	
	(function($){
	
		$(document).ready(function(){
			$('.recipe-items').slick({
				slidesToShow: 3,
				arrows: false,
				dots: true,
				autoplay: true,
				responsive: [
					{
					breakpoint: 993,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '80px',
						slidesToShow: 3,
						dots: false
					}
					},
				  {
					breakpoint: 768,
					settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '50px',
					slidesToShow: 3,
					dots: false,
						
					}
				  },
				  {
					breakpoint: 540,
					settings: {
					  arrows: false,
					  centerMode: true,
					  centerPadding: '40px',
					  slidesToShow: 3,
					  dots: false
					}
				  },
				  
				  {
					breakpoint: 480,
					settings: {
					  arrows: false,
					  centerMode: true,
					  centerPadding: '20px',
					  slidesToShow: 3,
					  dots: false
					}
				  }
				]
			  });

			  $('.products-wrapp').slick({
				centerMode: true,
				centerPadding: '60px',
				slidesToShow: 2,
				autoplaySpeed: 1500,
				arrows: false,
				dots: true,
				autoplay: true,
				responsive: [
				{
					breakpoint: 1224,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '80px',
						slidesToShow: 2,
						dots: true
					}
					},
					{
					breakpoint: 980,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '120px',
						slidesToShow: 2,
						dots: false
					}
					},
				  {
					breakpoint: 768,
					settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '80px',
					slidesToShow: 2,
					dots: false,
						
					}
				  },
				  {
					breakpoint: 540,
					settings: {
					  arrows: false,
					  centerMode: true,
					  centerPadding: '40px',
					  slidesToShow: 2,
					  dots: false
					}
				  },
				  
				  {
					breakpoint: 480,
					settings: {
					  arrows: false,
					  centerMode: true,
					  centerPadding: '20px',
					  slidesToShow: 2,
					  dots: false
					}
				  }
				]
			  })
			  $('.actual-wrapp').slick({
				slidesToShow: 3,
				arrows: false,
				dots: true,
				// autoplay: true,
				responsive: [
					{
					breakpoint: 991,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '80px',
						slidesToShow: 3,
						dots: false
					}
					},
				  {
					breakpoint: 768,
					settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '50px',
					slidesToShow: 3,
					dots: false,
						
					}
				  },
				  {
					breakpoint: 540,
					settings: {
					  arrows: false,
					  centerMode: true,
					  centerPadding: '30px',
					  slidesToShow: 2,
					  dots: false
					}
				  },
				  
				  {
					breakpoint: 480,
					settings: {
					  arrows: false,
					  centerMode: true,
					  centerPadding: '45px',
					  slidesToShow: 2,
					  dots: false
					}
				  }
				]
			  })
			  $('.pros-wrapp').slick({
				  slidesToShow: 1,
				  dots: true,
				  arrows: false,
				  autoplay: true,
				  autoplaySpeed: 2500,
			  })

		  });

		
	})(jQuery);
	
}
 