<?php


if (!function_exists('write_log')) {
  function write_log($log, $trace = true) {
    if (true === WP_DEBUG) {
      if (is_array($log) || is_object($log)) {
        error_log(print_r($log, true));
      } else {
        error_log($log);
      }
      if($trace){
	      
	      $backtrace = debug_backtrace();
	      $backtrace = $backtrace[0];
	      
	      unset($backtrace['function']);
	      unset($backtrace['args']);
	      
	      error_log( print_r( $backtrace, true ) );
	    }
      
    }
  }
}

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


function visibility_admin_notice() {
  ?>
    <?php if( 0 == get_option( 'blog_public' ) ):?>
	    <div class="notice notice-error">
	      <p><strong>Po uruchomieniu strony</strong> konieczna jest zmiana opcji "Widoczność dla wyszukiwarek" w <a href="<?php echo get_admin_url(null, 'options-reading.php');?>">Ustawienia/Czytanie</a></p>
	    </div>
    <?php endif;?>
  <?php
}
add_action( 'admin_notices', 'visibility_admin_notice' );


// Lazy Load
function prepare_lazyload_src($attr) {
  $attr['data-src'] = $attr['src'];
  $attr['src'] = '';
  $attr['class'] .= ' lazyload';
  return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'prepare_lazyload_src');


/*  Thumbnail upscale
/* ------------------------------------ */ 
function alx_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
  if ( !$crop ) return null; // let the wordpress default function handle this

  $aspect_ratio = $orig_w / $orig_h;
  $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

  $crop_w = round($new_w / $size_ratio);
  $crop_h = round($new_h / $size_ratio);

  $s_x = floor( ($orig_w - $crop_w) / 2 );
  $s_y = floor( ($orig_h - $crop_h) / 2 );

  return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter( 'image_resize_dimensions', 'alx_thumbnail_upscale', 10, 6 );
